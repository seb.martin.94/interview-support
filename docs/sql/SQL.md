# SQL

**SQL 1**

Table `T1`

| `Colonne_1` | `Colonne_2` |
| ----------- | ----------- |
| 1           | A           |
| 2           | B           |
| 3           | C           |

Table `T2`

| `Colonne_3` | `Colonne_4` |
| ----------- | ----------- |
| C           | x           |
| D           | y           |

```sql
select * from T1, T2
```

> 6, le produit cartésien (…du nombre de lignes de chaque table, doit 3 x 2)
> 
> Remarque : des consultants vont parfois répondre ‘le produit cartésien’…sans savoir donner le nombre. Il faut donc faire préciser.
>Cette question est extrêmement simple. Elle montre si le consultant à compris ce qu’était une base de données relationnelles.
>Beaucoup échoue cependant…


**SQL 2**

Table `Population`

| `Prenom`  | `Nom`    |
| --------- | -------- |
| Paul      | BERTRAND |
| Pierre    | LEROY    |
| Nathalie  | LEROY    |
| Jacques   | DUMONT   |
| Marie     | BERTRAND |
| Françoise | DUPONT   |
| Nathan    | MARTIN   |

Ecrire la requête qui retourne les prénoms des couples (hypothèse qu’il n’y a pas 2 couples ayant le même nom).

> Solution

```sql
Select P1.Prenom, P2.Prenom
From Population P1, Population P2
Where P1.Nom = P2.Nom 
And P1.Prenom <> P2.Prenom
```

> Commentaire : cette question peut déstabiliser le consultant car il doit appeler 2 fois la même table dans sa jointure.
