# Algorithmes

**Algorithme de la fonction factoriel**

> Solution 1 : Récursive

```
Entier Factoriel (entier n)
  Si n== 0 ou n == 1
    Retourner 1
  Sinon
    Retourner n x Factoriel (n-1)

```

> Solution 2 : Itérative

```
Entier Factoriel (entier n)
  Entier Resultat = 1 ;
  Si n > 1
    Tant que (n > 1 ; n--)
      Resultat = Resultat x n
  Fin Si
  Retourner Resultat
```

> Commentaire :
> Observer la manière dont le consultant construit son algorithme au fur et à mesure est riche d’enseignement.
> Discuter ensuite avec le consultant sur la plus performante des 2, les possibilités d’optimisation (résultats stockés en cache), etc.

**Comment coder une `HashMap`**

> TODO

**Iterator sur un Composite : Quelle structure de données pour un parcours en profondeur ? en largeur ?**

> - Parcours en profondeur : Pile
> - Parcours en largeur : File

