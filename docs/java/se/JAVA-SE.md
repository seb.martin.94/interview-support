
# Java SE

## JVM

**Options `-Xms`, `-Xmx` et `-Xss`**

> - `-Xms` Taille initiale de mémoire allouée à la JVM
> - `-Xmx` Taille maximale de mémoire allouée à la JVM
> - `-Xss` Taille de la pile de thread


**Qu'est-ce qu'un bytecode**

> Un bytecode est un code intermédiaire entre les instructions machines et le code source, qui n'est pas directement exécutable.
> Par extension, il désigne un flux d'octets binaire au format d'une classe java.
> Un bytecode est portable entre différentes architectures
> La compilation du bytecode en code machine natif est réalisée à la volée par la JVM

**Peut-on produire du bytecode Java avec d'autres langages ? Peut-on interpréter d'autre codes que du bytecode Java sur une JVM**

> - Scala, Groovy, Kotlin, ..., permettent de produire du bytecode
> - Une JVM n'interprète que du bytecode Java


**Qu'est que le ramasse miette. Quels types de ramasse miettes sont disponibles dans Java et pour quels cas d'utilisation sont-ils appropriés.**

> TODO!!

**Quels outils pour monitorer une JVM**

> JConsole `jconsole`, Java VisualVM `jvisualvm`.

## Java Language

**Citer les types primitifs**

> - Entier : `byte` (8 bits, signés), `short` (16 bits, signés), `int` (32 bits, signés), `long` (64 bits, signés)
> - Virgule flottante : `float` (32 bits, IEEE 754), `double` (64bits, IEEE 754)
> - Booléen : `boolean` (32 bits : `true`, `false`)
> - Caractère : `char` (16 bits, Unicode)

**Peut-on faire de l'héritance multiple en Java ? Comment peut-on le simuler ?**

> TODO

**Que font les mots clé suivants**

- `final`
- `finalize`
- `finally`

> TODO

**Qu'est-ce que l'auto-boxing**

> TODO

**Assert ?**

```java
String s1 = "a";
String s2 = "a";
String s3 = new String("a");

assert s1.equals(s2);
assert s1 == s2;

assert s1.equals(s3);
assert s1 == s3;
```

> Les 3 premières assertions sont vrais, la 4ème est fausse. La classe String gère un pool de string en interne.

**La méthode `protected` d'une classe `A` peut-elle être appelée par une classe `B` appartenant au même package que `A` si `B` n'hérite pas de `A` ?**

> Oui

## Java Collection

**Différence entre `Collection`, `List` et `Set`**

> - `Collection` est l'interface racine de la hiérarchie des collections. Représente un groupe d'objets désignés sous le nom d'élément.
> - `List` est une collection ordonnée, contenant une séquence d'éléments.
> - `Set` est une collection sans doublon au sens de la méthode `void equals(Object o)` des éléments du set. 
    Cette interface correspond à la notion d'ensemble au sens mathématique du terme.

**Citer différentes implémentations concrètes de `Set` et de `List`. Différences entre ces implémentations.**

> TODO

**Quelles différences entre `TreeSet` et `HashSet`**

> `HashSet` est une implémentation de `Set` basée sur une table de hachage (un `HashMap`).
> `TreeSet` est une implémentation de `Set` basée sur un arbre binaire (un `TreeMap`). `TreeSet` implémente l'interface `SortedSet`.


## Java Concurrence

**Un String, l'API `java.time` sont *Immutables*. Qu'est-ce qu'implique ce pattern et dans quel contexte est-il particulièrement utile**

> L'état d'un objet immutables ne peut pas être modifié après sa création. 
> Les méthode d'un immutable peuvent retourner de nouvelles instances. `String.replace(char, char)` par exemple. 
> Un objet immutable est  *thread safe*.


**Comment gérer des accès concurrents à une collection**

> ```
>List<String> list = Collections.synchronizedList(
>        new ArrayList()
>);
> ```

**Quel est l'intérêt d'une classe *Stateless* ?**

> Une classe stateless ne possède pas de variable d'instance.
> Un objet stateless peut intégrer un pool d'objets réutilisables sans risquer d'effet de bord.

**Qu'est-ce qu'un Semaphore ?**

> TODO


## Java Generics

**Quelles lignes compilent ?**

```java
Object[] arr = new String[3];
List<Object> list = new ArrayList<String>();
```

> Ligne 1 OK (covariance des tableaux), mais ligne 2 KO.
  
**Quelles lignes compilent ?**

```java
List<?> list = new ArrayList<String>();
list.add("");
list.add(new Object());
list.add(null);
```

> Ligne 1 OK, ligne 2 KO, ligne 3 KO, ligne 4 OK.
  
**Quelles lignes compilent ?**

```java
List<? extends String> list = Arrays.asList("a");
list.get(0);
list.add("");
list.add(null);
```

> Ligne 1 OK, ligne 2 OK, ligne 3 KO, ligne 4 OK.
> La ligne 4 provoquera une `UnsupportedOperationException` à l'exécution : l'implémentation de `List` retournée par `Arrays.asList("a")` est de taille fixe. 


**Quelles lignes compilent ?**

```java
List<? super String> list = new ArrayList<String>();
list.add("");
list.add(new Object());
String s = list.get(0);
Object o = list.get(0);
```

> Ligne 1 OK, ligne 2 OK, ligne 3 KO, ligne 4 KO, ligne 5 OK.

**Rendre générique**

```java
static void add(Collection c, Object o) {
    c.add(o);
}
```

> Solution

```java
static <T> void add(Collection<T> c, T o) {
    c.add(o);
}
```

**Compile ?**

```java
class Main {
    public void p( Collection<Integer> c){ } 
    public void p( Collection<String> c){ }
}
```

> Ne compile pas (les deux signatures équivalent à `public void p( Collection c){ }` dans le bytecode)


## Java IO

**Quelle différence entre un `InputStream` et un `Reader`**

> Un `InputStream` permet de lire des `byte`, un `Reader` permet de lire des `char`.


***Try with resource*, lire la première ligne d'un fichier**

> Solution

```java
static String readFirstLineFromFile(String path) throws IOException {

    try (BufferedReader br =
                   new BufferedReader(new FileReader(path))) {
        return br.readLine();
    }
}
```



## Introspection

**Que fait la méthode ?**

```java
static void print(Object o) {

    Class<?> c = o.getClass();

    for (Method m : c.getMethods()){

        String methodString = m.getName();
        System.out.println("Name: " + methodString);

        String returnString = m.getReturnType().getName();
        System.out.println(" Return Type: " + returnString);
        
        System.out.print(" Parameter Types:");

        for (Class<?> p : m . getParameterTypes() ) {
            
            String parameterString = p.getName();
            System.out.print(" " + parameterString);
        }

        System.out.println();
    }
}    
```

> Affiche les méthodes d'un objet avec leur nom, leur type de retour et led types de leurs paramètres. 


## Java Annotation

**Que signifie cette déclaration**

```java
@Documented
@Target({TYPE, METHOD, CONSTRUCTOR})
@Retention(RetentionPolicy.SOURCE)
@interface Author {
    String name();
    int year();
}
```

## Java Stream & Lambda

**Qu'est-ce qui caractérise une interface fonctionnelle, quel rapport avec les Lambdas**

> Une interface fonctionnelle est une interface ne possédants qu'une méthode.
> L'annotation `@FunctionalInterface` peut décorer une interface pour prévenir le non respect du contrat.
> Les interfaces fonctionnelles peuvent être implémentées par des lambdas.

**Qu'est-ce qu'un Consumer ? Qu'est-ce qu'une Function ? Existe-t'il une différence entre les deux ?**

> Un Consumer est une interface fonctionnelle utilisée dans l'API Stream. La signature de sa méthode abstraite est `void accept(T t)`.
> Une Function est une interface fonctionnelle utilisée dans l'API Stream. La signature de sa méthode abstraite est `R apply(T t)`.
> La méthode abstraite de Function retourne une valeur tandis que celle de Consumer n'en retourne pas.


**Rédiger une lambda BiFunction réalisant une addition**

> Solution

```java
BiFunction<Integer, Integer, Integer> add = (a, b) -> a + b;

assert add.apply(1, 2) == 3;
```



**Utiliser un stream pour afficher le carré des int pairs de 1 à 10**

```java
IntStream.rangeClosed(1, 10)
    .filter(n -> n % 2 == 0)
    .map(n -> n * n)
    .forEach(c -> System.out.println(c));
```

**Les opérations sur les Stream sont divisées en opérations *intermédiaires* et *terminales*. Des exemples ?**

> intermédiaires : map, filter, peek, ...
> terminales : collect, reduce, max, ...


## Java 9

- modules
- ...

## Java 10

- ...

