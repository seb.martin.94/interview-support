# Programmation Orientée Objet (POO)

**Encapsulation, Héritage, Polymorphisme**

> TODO

**Les différents polymorphismes, *Overload* vs *Override***

> - Polymorphisme ad'hoc : Surcharge (Overloading), résolu statiquement à la compilation.
> - Polymorphisme d'inclusion : Redéfinition, masquage (Overriding), résolu dynamiquement.
> - Polymorphisme paramétrique ou généricité : résolu statiquement.


**Observer**

> TODO

**Quel pattern pour modéliser un arbre**

> `Composite`

**Quels patterns pour parcourir les noeuds d'un `Composite`**

> `Iterator`, `Interpreter`, `Visitor`

**`Iterator` vs `Interpreter` vs `Visitor`**

> TODO

**`Adapter`, `Proxy`, `Decorator` utilisent tous les 3 la délégation. Qu'est-ce qui les différencies**

> TODO

**Inversion de contrôle vs Injection de dépendance. Intérêt de l'IoC**

> Le principe de l'IoC est de déléguer la création des objets dont dépend une classe aux clients instanciant des objets de cette classe.
> L'injection de dépendance permet d'instancier des objets avec inversion de contrôle par configuration.
> En typant les dépendances d'une classe avec des interfaces, on obtient un couplage faible.
> Les implémentations des dépendances peuvent facilement être interverties.
> Les tests sont simplifiés en exploitant des techniques de mock.

