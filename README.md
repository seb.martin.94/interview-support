# Supports d'interviews

Des questionnaires permettant de challenger des développeurs.

- [Programmation Orientée Objet](docs/poo/POO.md)
- [Algorithmes](docs/algo/ALGO.md)
- [Architecture Logicielle](docs/arch/log/ARCH-LOG.md)
- [Architecture Système](docs/arch/sys/ARCH-SYS.md)
- Java
  - [Java SE](docs/java/se/JAVA-SE.md)
  - [Java Build](docs/java/build/JAVA-BUILD.md)
  - [Java EE](docs/java/ee/JAVA-EE.md)
